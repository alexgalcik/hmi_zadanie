#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QTimer"
#include "QPainter"
#include "math.h"
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgcodecs.hpp"
#include <QCoreApplication>
#include <QtConcurrent/QtConcurrent>
#include "stdio.h"
#include "QKeyEvent"

//funkcia local robot je na priame riadenie robota, ktory je vo vasej blizskoti, viete si z dat ratat polohu atd (zapnutie dopravneho oneskorenia sposobi opozdenie dat oproti aktualnemu stavu robota)
void MainWindow::localrobot(TKobukiData &sens)
{
    ///PASTE YOUR CODE HERE
    /// ****************
    ///
    ///
    /// ****************
    ///
    ///
    ///
    ///
    ///
    ///



    if(prvyStart)
    {

        GyroUholOld=sens.GyroAngle;
        PomEncoderL= sens.EncoderLeft;
        PomEncoderR= sens.EncoderRight;


        deltaUhol=0.0;
        prvyStart=false;
    }



    PomEncoderL=sens.EncoderLeft;
    if(dl%10==0)
    {
        ///toto je skaredy kod. rozumne je to posielat do ui cez signal slot..

    }
    dl++;

}

double degreeToRad(double degree){
    return degree * (M_PI/180.0);
}

double radToDegree(double rad){
    return rad * (180/M_PI);
}



// funkcia local laser je naspracovanie dat z lasera(zapnutie dopravneho oneskorenia sposobi opozdenie dat oproti aktualnemu stavu robota)
int MainWindow::locallaser(LaserMeasurement &laserData)
{

    paintThisLidar(laserData);


    //priklad ako zastavit robot ak je nieco prilis blizko
    if(laserData.Data[0].scanDistance<500)
    {

        sendRobotCommand(ROBOT_STOP);
    }
    ///PASTE YOUR CODE HERE
    /// ****************
    /// mozne hodnoty v return
    /// ROBOT_VPRED
    /// ROBOT_VZAD
    /// ROBOT_VLAVO
    /// ROBOT_VPRAVO
    /// ROBOT_STOP
    /// ROBOT_ARC
    ///
    /// ****************

    return -1;
}


//--autonomousrobot simuluje slucku robota, ktora bezi priamo na robote
// predstavte si to tak,ze ste naprogramovali napriklad polohovy regulator, uploadli ste ho do robota a tam sa to vykonava
// dopravne oneskorenie nema vplyv na data
void MainWindow::autonomousrobot(TKobukiData &sens)
{
    ///PASTE YOUR CODE HERE
    /// ****************
    ///
    ///
    /// ****************
}
//--autonomouslaser simuluje spracovanie dat z robota, ktora bezi priamo na robote
// predstavte si to tak,ze ste naprogramovali napriklad sposob obchadzania prekazky, uploadli ste ho do robota a tam sa to vykonava
// dopravne oneskorenie nema vplyv na data
int MainWindow::autonomouslaser(LaserMeasurement &laserData)
{
    ///PASTE YOUR CODE HERE
    /// ****************
    ///
    ///
    /// ****************
    return -1;
}

///kamera nema svoju vlastnu funkciu ktora sa vola, ak chcete niekde pouzit obrazok, aktualny je v premennej
/// robotPicture alebo ekvivalent AutonomousrobotPicture
/// pozor na synchronizaciu, odporucam akonahle chcete robit nieco s obrazkom urobit si jeho lokalnu kopiu
/// cv::Mat frameBuf; robotPicture.copyTo(frameBuf);


//sposob kreslenia na obrazovku, tento event sa spusti vzdy ked sa bud zavola funkcia update() alebo operacny system si vyziada prekreslenie okna

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setBrush(Qt::black);
    QPen pero;
    pero.setStyle(Qt::SolidLine);
    pero.setWidth(3);
    pero.setColor(Qt::green);
    QRect rect(1,1,1000,1000);
    rect= ui->frame->geometry();
    painter.drawRect(rect);
    QRect rect1(1,1,10,10);
    rect1= ui->frame_2->geometry();
    painter.drawRect(rect1);
//    QRect rect2(1,1,10,10);
//    rect2= ui->frame_3->geometry();
//    painter.drawRect(rect2);
    if(updateCameraPicture==1)
    {

        for(int k=0;k<paintLaserData.numberOfScans;k++)
        {
            double dist = paintLaserData.Data[k].scanDistance/1000;
            double angle = ((360.0-paintLaserData.Data[k].scanAngle)*3.14159/180.0);

                if(qRadiansToDegrees(angle)<(54/2) || qRadiansToDegrees(angle)> (360-(54/2)) ){

                double f = 628.036;
                double yDist = 6/100.0;
                double z = dist * cos(angle);
                double xDist = dist * sin(angle);

                int xp=robotPicture.cols/2 - ((f * xDist)/z);
                int yp=robotPicture.rows/2+((f * yDist)/z );
                cv::Rect  rectCV;

                    //cv::circle( robotPicture, cv::Point(xp, yp),   1,  cv::Scalar(0,0,0),   2,  cv::FILLED,   0 );
                if(robotPicture.cols>xp && xp>0 && robotPicture.rows>yp && yp>0 ){
                    int color = 10+dist*100 > 255 ? 255 : 10+dist*100;
                         cv::floodFill(robotPicture, cv::Point(xp, yp),  cv::Scalar(3,color,252),&rectCV, cv::Scalar(5,5,5),   cv::Scalar(5, 5,5), 4);
                         if (dist < 0.6){
                             cv::putText(robotPicture,"Warning!", cv::Point(robotPicture.cols/2.0-40, 50),cv::FONT_HERSHEY_DUPLEX,1.0, CV_RGB(0,0,0),6);
                             cv::putText(robotPicture,"Warning!", cv::Point(robotPicture.cols/2.0-40, 50),cv::FONT_HERSHEY_DUPLEX,1.0, CV_RGB(255,0,0),2);
                         }
                }
         }
        }
        updateCameraPicture=0;

        QImage imgIn= QImage((uchar*) robotPicture.data, robotPicture.cols, robotPicture.rows, robotPicture.step, QImage::Format_BGR888);


//        for (int x = 0; x < 10; ++x) {
//                    for (int y = 0; y < 10; ++y) {
//                        imgIn.setPixel(imgIn.width()/2+x, imgIn.height()/4+y, qRgb(255, 0, 0));
//                    }
//                }
      //  cv::imshow("client",robotPicture);
       //   painter.setPen(pero);


     painter.drawImage(rect, imgIn);

    } else
    {
        QImage imgIn= QImage((uchar*) robotPicture.data, robotPicture.cols, robotPicture.rows, robotPicture.step, QImage::Format_BGR888);

//        for (int x = 0; x < 10; ++x) {
//                    for (int y = 0; y < 10; ++y) {
//                        imgIn.setPixel(imgIn.width()/2+x, imgIn.height()/4+y, qRgb(255, 0, 0));
//                    }
//                }
         painter.drawImage(rect,imgIn);
    }


    if(updateLaserPicture==1 )
    {
        /// ****************
        ///you can change pen or pen color here if you want
        /// ****************
        ///
        pero.setColor(Qt::lightGray);
        painter.setPen(pero);
       int  robotOffset = 10;
        painter.drawEllipse(QPoint(rect1.width()-(rect1.width()/2)+rect1.topLeft().x(), rect1.height()-(rect1.height()/2)+rect1.topLeft().y()-robotOffset),15,15);
        painter.drawLine(QPoint(rect1.width()-(rect1.width()/2)+rect1.topLeft().x(), rect1.height()-(rect1.height()/2)+rect1.topLeft().y()-robotOffset),
                         QPoint(rect1.width()-(rect1.width()/2)+rect1.topLeft().x(), rect1.height()-(rect1.height()/2)+rect1.topLeft().y()-15-robotOffset));
        for(int k=0;k<paintLaserData.numberOfScans;k++)
        {

            float distanceInMeters = paintLaserData.Data[k].scanDistance/1000;
            if(distanceInMeters>0){
                int dist=paintLaserData.Data[k].scanDistance/12;
                int xp=rect1.width()-(rect1.width()/2+dist*sin((360.0-paintLaserData.Data[k].scanAngle)*3.14159/180.0))+rect1.topLeft().x();
                int yp=rect1.height()-(rect1.height()/2+dist*cos((360.0-paintLaserData.Data[k].scanAngle)*3.14159/180.0))+rect1.topLeft().y();
                if(rect1.contains(xp,yp)){
                    if(distanceInMeters < 0.7){
                         pero.setColor(Qt::red);
                         painter.setPen(pero);
                    }else{
                        pero.setColor(Qt::lightGray);
                        painter.setPen(pero);
                    }
                    painter.drawEllipse(QPoint(xp, yp),1,1);

                }
                float offset = 0.012;
                float level1 = 1;
                float level2 = 0.7;
                float level3 = 0.4;
                //up
                pero.setColor(Qt::black);
                painter.setPen(pero);

                if(distanceInMeters < level1 && (360.0-paintLaserData.Data[k].scanAngle)<45/2 &&(360.0-paintLaserData.Data[k].scanAngle)> -45/2 ){
                painter.drawLine(QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset),
                                 QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset));
                if(distanceInMeters < level2)
                painter.drawLine(QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*2),
                                 QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*2));
                if(distanceInMeters < level3)
                painter.drawLine(QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*3),
                                 QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*3));

            }else{
//                    painter.drawLine(QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset),
//                                     QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset));
//                    painter.drawLine(QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*2),
//                                     QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*2));
//                    painter.drawLine(QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*3),
//                                     QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*3));
                }

            if(distanceInMeters < level1 && (360.0-paintLaserData.Data[k].scanAngle)<45/2+45*4 &&(360.0-paintLaserData.Data[k].scanAngle)> 45/2 +45*3){
                //down
                painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset),
                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset));
                if(distanceInMeters < level2)
                painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*2),
                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*2));
                if(distanceInMeters < level3)
                painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*3),
                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*3));
            }else{
//                painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset),
//                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset));
//                painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*2),
//                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*2));
//                painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*3),
//                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*3));
            }

             if(distanceInMeters < level1 && (360.0-paintLaserData.Data[k].scanAngle)<45/2+45*6 &&(360.0-paintLaserData.Data[k].scanAngle)> 45/2 +45*5){
                //right
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset, rect.topLeft().y()+rect.height()*0.2),
                                 QPoint(rect.topRight().x()-rect.height()*offset, rect.bottomLeft().y()-rect.height()*0.2));
                if(distanceInMeters < level2)
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*2, rect.topLeft().y()+rect.height()*0.2),
                                 QPoint(rect.topRight().x()-rect.height()*offset*2, rect.bottomLeft().y()-rect.height()*0.2));
                if(distanceInMeters < level3)
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*3, rect.topLeft().y()+rect.height()*0.2),
                                 QPoint(rect.topRight().x()-rect.height()*offset*3, rect.bottomLeft().y()-rect.height()*0.2));
               }else{
//                 painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset, rect.topLeft().y()+rect.height()*0.2),
//                                  QPoint(rect.topRight().x()-rect.height()*offset, rect.bottomLeft().y()-rect.height()*0.2));
//                 painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*2, rect.topLeft().y()+rect.height()*0.2),
//                                  QPoint(rect.topRight().x()-rect.height()*offset*2, rect.bottomLeft().y()-rect.height()*0.2));
//                 painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*3, rect.topLeft().y()+rect.height()*0.2),
//                                  QPoint(rect.topRight().x()-rect.height()*offset*3, rect.bottomLeft().y()-rect.height()*0.2));
             }
              if(distanceInMeters < level1 && (360.0-paintLaserData.Data[k].scanAngle)<45/2+45*2 &&(360.0-paintLaserData.Data[k].scanAngle)> 45/2 +45*1){

             //left
             painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset, rect.topLeft().y()+rect.height()*0.2),
                              QPoint(rect.topLeft().x()+rect.height()*offset, rect.bottomLeft().y()-rect.height()*0.2));
             if(distanceInMeters < level2)
             painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset*2, rect.topLeft().y()+rect.height()*0.2),
                              QPoint(rect.topLeft().x()+rect.height()*offset*2, rect.bottomLeft().y()-rect.height()*0.2));
             if(distanceInMeters < level3)
             painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset*3, rect.topLeft().y()+rect.height()*0.2),
                              QPoint(rect.topLeft().x()+rect.height()*offset*3, rect.bottomLeft().y()-rect.height()*0.2));
              }else{
//                  painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset, rect.topLeft().y()+rect.height()*0.2),
//                                   QPoint(rect.topLeft().x()+rect.height()*offset, rect.bottomLeft().y()-rect.height()*0.2));
//                  painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset*2, rect.topLeft().y()+rect.height()*0.2),
//                                   QPoint(rect.topLeft().x()+rect.height()*offset*2, rect.bottomLeft().y()-rect.height()*0.2));
//                  painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset*3, rect.topLeft().y()+rect.height()*0.2),
//                                   QPoint(rect.topLeft().x()+rect.height()*offset*3, rect.bottomLeft().y()-rect.height()*0.2));
              }
               if(distanceInMeters < level1 && (360.0-paintLaserData.Data[k].scanAngle)<45/2+45*1&&(360.0-paintLaserData.Data[k].scanAngle)> 45/2 ){
                //left upper
                painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset, rect.topLeft().y()+rect.height()*0.2),
                                  QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset));
                if(distanceInMeters < level2)
                painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset*2, rect.topLeft().y()+rect.height()*0.2),
                                  QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*2));
                if(distanceInMeters < level3)
                painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset*3, rect.topLeft().y()+rect.height()*0.2),
                                  QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*3));
            }else{
//                   painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset, rect.topLeft().y()+rect.height()*0.2),
//                                     QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset));
//                   painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset*2, rect.topLeft().y()+rect.height()*0.2),
//                                     QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*2));
//                   painter.drawLine(QPoint(rect.topLeft().x()+rect.height()*offset*3, rect.topLeft().y()+rect.height()*0.2),
//                                     QPoint(rect.topLeft().x()+rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*3));
               }
            if(distanceInMeters < level1 && (360.0-paintLaserData.Data[k].scanAngle)<45/2+45*7&&(360.0-paintLaserData.Data[k].scanAngle)> 45/2+6*45 ){
                //right upper
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset, rect.topLeft().y()+rect.height()*0.2),
                               QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset));
                if(distanceInMeters < level2)
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*2, rect.topLeft().y()+rect.height()*0.2),
                                  QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*2));
                if(distanceInMeters < level3)
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*3, rect.topLeft().y()+rect.height()*0.2),
                                 QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*3));
            }else{
//                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset, rect.topLeft().y()+rect.height()*0.2),
//                               QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset));
//                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*2, rect.topLeft().y()+rect.height()*0.2),
//                                  QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*2));
//                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*3, rect.topLeft().y()+rect.height()*0.2),
//                                 QPoint(rect.topRight().x()-rect.width()*0.2, rect.topLeft().y()+rect.height()*offset*3));
            }
            if(distanceInMeters < level1 && (360.0-paintLaserData.Data[k].scanAngle)<45/2+45*5&&(360.0-paintLaserData.Data[k].scanAngle)> 45/2+4*45 ){
                //right bottom
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset, rect.bottomLeft().y()-rect.height()*0.2),
                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset));
               if(distanceInMeters < level2)
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*2, rect.bottomLeft().y()-rect.height()*0.2),
                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*2));
               if(distanceInMeters < level3)
                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*3, rect.bottomLeft().y()-rect.height()*0.2),
                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*3));
            }else{
//                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset, rect.bottomLeft().y()-rect.height()*0.2),
//                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset));
//                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*2, rect.bottomLeft().y()-rect.height()*0.2),
//                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*2));
//                painter.drawLine(QPoint(rect.topRight().x()-rect.height()*offset*3, rect.bottomLeft().y()-rect.height()*0.2),
//                                 QPoint(rect.bottomRight().x()-rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*3));
            }
               if(distanceInMeters < level1 && (360.0-paintLaserData.Data[k].scanAngle)<45/2+45*3&&(360.0-paintLaserData.Data[k].scanAngle)> 45/2+2*45 ){
                //left bottom
              painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset),
                           QPoint(rect.topLeft().x()+rect.height()*offset, rect.bottomLeft().y()-rect.height()*0.2));
               if(distanceInMeters < level2)
              painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*2),
                              QPoint(rect.topLeft().x()+rect.height()*offset*2, rect.bottomLeft().y()-rect.height()*0.2));
                 if(distanceInMeters < level3)
                     painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*3),
                                       QPoint(rect.topLeft().x()+rect.height()*offset*3, rect.bottomLeft().y()-rect.height()*0.2));

               }else{
//                   painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset),
//                                QPoint(rect.topLeft().x()+rect.height()*offset, rect.bottomLeft().y()-rect.height()*0.2));
//                   painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*2),
//                                   QPoint(rect.topLeft().x()+rect.height()*offset*2, rect.bottomLeft().y()-rect.height()*0.2));
//                   painter.drawLine(QPoint(rect.bottomLeft().x()+rect.width()*0.2, rect.bottomLeft().y()-rect.height()*offset*3),
//                                    QPoint(rect.topLeft().x()+rect.height()*offset*3, rect.bottomLeft().y()-rect.height()*0.2));
               }
            }
        }
    }

        float skeletondistFirst = sqrt(pow(kostricka.joints[8].x - kostricka.joints[4].x, 2) +  pow(kostricka.joints[8].y -kostricka.joints[4].y, 2));
        float skeletondistSecond = sqrt(pow(kostricka.joints[12].x - kostricka.joints[4].x, 2) +  pow(kostricka.joints[12].y -kostricka.joints[4].y, 2));
        float skeletonSecondIndexAngle = radToDegree(atan2( (kostricka.joints[29].y - kostricka.joints[26].y), (kostricka.joints[29].x - kostricka.joints[26].x)))+90;
        if( skeletonSecondIndexAngle == 90 )
            skeletonSecondIndexAngle = 0;
        char tt=0x01;
       if(skeletondistFirst<0.1 && skeletondistFirst>0){
           skeletonEnabled = true;
           tt=0x01;
           sendRobotCommand(tt,250);  //dopredu
       }else if(skeletondistSecond<0.1 && skeletondistSecond>0){
            skeletonEnabled = true;
           tt=0x02;
           sendRobotCommand(tt,-250);     //dozadu
       }else if (skeletonSecondIndexAngle > 22.5 && skeletonSecondIndexAngle < 180 ){
            skeletonEnabled = true;
           tt=0x04;
           sendRobotCommand(tt,3.14159/4);  //vlavo
       }else if (skeletonSecondIndexAngle < -22.5 && skeletonSecondIndexAngle > -180 ){
            skeletonEnabled = true;
           tt=0x03;
           sendRobotCommand(tt,-3.14159/4);   //vpravo
       }else if(skeletonEnabled == true){
           tt=0x00;
           sendRobotCommand(tt);
           skeletonEnabled = false; //stop
       }
}



///konstruktor aplikacie, nic co tu je nevymazavajte, ak potrebujete, mozete si tu pridat nejake inicializacne parametre
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    robotX=0;
    robotY=0;
    robotFi=0;

    showCamera=true;
    showLidar=true;
    showSkeleton=false;
    applyDelay=false;
    dl=0;
    stopall=1;
    prvyStart=true;
    updateCameraPicture=0;
    ipaddress="127.0.0.1";
    std::function<void(void)> f =std::bind(&robotUDPVlakno, (void *)this);
    robotthreadHandle=std::move(std::thread(f));
    std::function<void(void)> f2 =std::bind(&laserUDPVlakno, (void *)this);
    laserthreadHandle=std::move(std::thread(f2));


    std::function<void(void)> f3 =std::bind(&skeletonUDPVlakno, (void *)this);
    skeletonthreadHandle=std::move(std::thread(f3));

    //--ak by ste nahodou chceli konzolu do ktorej mozete vypisovat cez std::cout, odkomentujte nasledujuce dva riadky
   // AllocConsole();
   // freopen("CONOUT$", "w", stdout);


    QFuture<void> future = QtConcurrent::run([=]() {
        imageViewer();
        // Code in this block will run in another thread
    });



        Imager.start();

}

///funkcia co sa zavola ked stlacite klavesu na klavesnici..
/// pozor, ak niektory widget akceptuje klavesu, sem sa nemusite (ale mozete) dostat
/// zalezi na to ako konkretny widget spracuje svoj event
void MainWindow::keyPressEvent(QKeyEvent* event)
{
    //pre pismena je key ekvivalent ich ascii hodnoty
    //pre ine klavesy pozrite tu: https://doc.qt.io/qt-5/qt.html#Key-enum
    char key =  char(event->key());
    char tt=0x01;
    printf("%c \n",key);
    switch(key){
        case 'W':
                tt=0x01;
                sendRobotCommand(tt,250);
                break;
        case 'S':
                tt=0x02;
                sendRobotCommand(tt,-250);
                break;
        case 'A':
                tt=0x04;
                sendRobotCommand(tt,3.14159/4);
                break;
        case 'D':
              tt=0x03;
              sendRobotCommand(tt,-3.14159/4);
             break;
    case 'Q':
        tt=0x00;
        sendRobotCommand(tt);
         break;


    default:
        break;

    }

}
































































MainWindow::~MainWindow()
{
    stopall=0;
    laserthreadHandle.join();
    robotthreadHandle.join();
    skeletonthreadHandle.join();
    delete ui;
}









void MainWindow::robotprocess()
{
#ifdef _WIN32
    WSADATA wsaData = {0};
    int iResult = 0;
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
#endif
    rob_slen = sizeof(las_si_other);
    if ((rob_s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {

    }

    char rob_broadcastene=1;
    DWORD timeout=100;

    setsockopt(rob_s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof timeout);
    setsockopt(rob_s,SOL_SOCKET,SO_BROADCAST,&rob_broadcastene,sizeof(rob_broadcastene));
    // zero out the structure
    memset((char *) &rob_si_me, 0, sizeof(rob_si_me));

    rob_si_me.sin_family = AF_INET;
    rob_si_me.sin_port = htons(53000);
    rob_si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    rob_si_posli.sin_family = AF_INET;
    rob_si_posli.sin_port = htons(5300);
    rob_si_posli.sin_addr.s_addr =inet_addr(ipaddress.data());//inet_addr("10.0.0.1");// htonl(INADDR_BROADCAST);
    rob_slen = sizeof(rob_si_me);
    bind(rob_s , (struct sockaddr*)&rob_si_me, sizeof(rob_si_me) );

    std::vector<unsigned char> mess=robot.setDefaultPID();
    if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
    {

    }
#ifdef _WIN32
    Sleep(100);
#else
    usleep(100*1000);
#endif
    mess=robot.setSound(440,1000);
    if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
    {

    }
    unsigned char buff[50000];
    while(stopall==1)
    {

        memset(buff,0,50000*sizeof(char));
        if ((rob_recv_len = recvfrom(rob_s, (char*)&buff, sizeof(char)*50000, 0, (struct sockaddr *) &rob_si_other, &rob_slen)) == -1)
        {

            continue;
        }
        //tu mame data..zavolame si funkciu

        //     memcpy(&sens,buff,sizeof(sens));
        struct timespec t;
        //      clock_gettime(CLOCK_REALTIME,&t);

        int returnval=robot.fillData(sens,(unsigned char*)buff);
        if(returnval==0)
        {
            //     memcpy(&sens,buff,sizeof(sens));

            std::chrono::steady_clock::time_point timestampf=std::chrono::steady_clock::now();

            autonomousrobot(sens);

            if(applyDelay==true)
            {
                struct timespec t;
                RobotData newcommand;
                newcommand.sens=sens;
                //    memcpy(&newcommand.sens,&sens,sizeof(TKobukiData));
                //        clock_gettime(CLOCK_REALTIME,&t);
                newcommand.timestamp=std::chrono::steady_clock::now();;//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
                auto timestamp=std::chrono::steady_clock::now();;//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
                sensorQuerry.push_back(newcommand);
                for(int i=0;i<sensorQuerry.size();i++)
                {
                    if(( std::chrono::duration_cast<std::chrono::nanoseconds>(timestampf-sensorQuerry[i].timestamp)).count()>(2.5*1000000000))
                    {
                        localrobot(sensorQuerry[i].sens);
                        sensorQuerry.erase(sensorQuerry.begin()+i);
                        i--;
                        break;

                    }
                }

            }
            else
            {
                sensorQuerry.clear();
                localrobot(sens);
            }
        }


    }

    std::cout<<"koniec thread2"<<std::endl;
}
/// vravel som ze vas to nemusi zaujimat. tu nic nieje
/// nosy litlle bastard
void MainWindow::laserprocess()
{
#ifdef _WIN32
    WSADATA wsaData = {0};
    int iResult = 0;
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
#endif
    las_slen = sizeof(las_si_other);
    if ((las_s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {

    }

    char las_broadcastene=1;
#ifdef _WIN32
    DWORD timeout=100;

    setsockopt(las_s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof timeout);
    setsockopt(las_s,SOL_SOCKET,SO_BROADCAST,&las_broadcastene,sizeof(las_broadcastene));
#else
    setsockopt(las_s,SOL_SOCKET,SO_BROADCAST,&las_broadcastene,sizeof(las_broadcastene));
#endif
    // zero out the structure
    memset((char *) &las_si_me, 0, sizeof(las_si_me));

    las_si_me.sin_family = AF_INET;
    las_si_me.sin_port = htons(52999);
    las_si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    las_si_posli.sin_family = AF_INET;
    las_si_posli.sin_port = htons(5299);
    las_si_posli.sin_addr.s_addr = inet_addr(ipaddress.data());;//htonl(INADDR_BROADCAST);
    bind(las_s , (struct sockaddr*)&las_si_me, sizeof(las_si_me) );
    char command=0x00;
    if (sendto(las_s, &command, sizeof(command), 0, (struct sockaddr*) &las_si_posli, rob_slen) == -1)
    {

    }
    LaserMeasurement measure;
    while(stopall==1)
    {

        if ((las_recv_len = recvfrom(las_s, (char *)&measure.Data, sizeof(LaserData)*1000, 0, (struct sockaddr *) &las_si_other, &las_slen)) == -1)
        {

            continue;
        }
        measure.numberOfScans=las_recv_len/sizeof(LaserData);
        //tu mame data..zavolame si funkciu

        //     memcpy(&sens,buff,sizeof(sens));
        int returnValue=autonomouslaser(measure);

        if(applyDelay==true)
        {
            struct timespec t;
            LidarVector newcommand;
            memcpy(&newcommand.data,&measure,sizeof(LaserMeasurement));
            //    clock_gettime(CLOCK_REALTIME,&t);
            newcommand.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
            auto timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
            lidarQuerry.push_back(newcommand);
            for(int i=0;i<lidarQuerry.size();i++)
            {
                if((std::chrono::duration_cast<std::chrono::nanoseconds>(timestamp-lidarQuerry[i].timestamp)).count()>(2.5*1000000000))
                {
                    returnValue=locallaser(lidarQuerry[i].data);
                    if(returnValue!=-1)
                    {
                        //sendRobotCommand(returnValue);
                    }
                    lidarQuerry.erase(lidarQuerry.begin()+i);
                    i--;
                    break;

                }
            }

        }
        else
        {


            returnValue=locallaser(measure);
            if(returnValue!=-1)
            {
                //sendRobotCommand(returnValue);
            }
        }
    }
    std::cout<<"koniec thread"<<std::endl;
}

void MainWindow::on_pushButton_3_clicked()
{
    char tt=0x01;
    sendRobotCommand(tt,250);
}

void MainWindow::on_pushButton_7_clicked()
{
    char tt=0x00;
    sendRobotCommand(tt);

}

void MainWindow::on_pushButton_5_clicked()
{
    char tt=0x02;
    sendRobotCommand(tt,-250);
}


void MainWindow::on_pushButton_4_clicked()
{
    char tt=0x04;
    sendRobotCommand(tt,3.14159/4);
}

void MainWindow::on_pushButton_6_clicked()
{
    char tt=0x03;
    sendRobotCommand(tt,-3.14159/4);
}

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::sendRobotCommand(char command,double speed,int radius)
{
    globalcommand=command;
 //   if(applyDelay==false)
    {

        std::vector<unsigned char> mess;
        switch(command)
        {
        case  ROBOT_VPRED:
            mess=robot.setTranslationSpeed(speed);
            break;
        case ROBOT_VZAD:
            mess=robot.setTranslationSpeed(speed);
            break;
        case ROBOT_VLAVO:
            mess=robot.setRotationSpeed(speed);
            break;
        case ROBOT_VPRAVO:
            mess=robot.setRotationSpeed(speed);
            break;
        case ROBOT_STOP:
            mess=robot.setTranslationSpeed(0);
            break;
        case ROBOT_ARC:
            mess=robot.setArcSpeed(speed,radius);
            break;


        }
        if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
        {

        }
    }
  /*  else
    {
        struct timespec t;
        RobotCommand newcommand;
        newcommand.command=command;
        newcommand.radius=radius;
        newcommand.speed=speed;
        //clock_gettime(CLOCK_REALTIME,&t);
        newcommand.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
        commandQuery.push_back(newcommand);
    }*/
}
/*void MainWindow::autonomousRobotCommand(char command,double speed,int radius)
{
    return;
    std::vector<unsigned char> mess;
    switch(command)
    {
    case  ROBOT_VPRED:
        mess=robot.setTranslationSpeed(speed);
        break;
    case ROBOT_VZAD:
        mess=robot.setTranslationSpeed(speed);
        break;
    case ROBOT_VLAVO:
        mess=robot.setRotationSpeed(speed);
        break;
    case ROBOT_VPRAVO:
        mess=robot.setRotationSpeed(speed);
        break;
    case ROBOT_STOP:
        mess=robot.setTranslationSpeed(0);
        break;
    case ROBOT_ARC:
        mess=robot.setArcSpeed(speed,radius);
        break;

    }
    if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
    {

    }
}
void MainWindow::robotexec()
{


    if(applyDelay==true)
    {
        struct timespec t;

        // clock_gettime(CLOCK_REALTIME,&t);
        auto timestamp=std::chrono::steady_clock::now();;//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
        for(int i=0;i<commandQuery.size();i++)
        {
       //     std::cout<<(std::chrono::duration_cast<std::chrono::nanoseconds>(timestamp-commandQuery[i].timestamp)).count()<<std::endl;
            if((std::chrono::duration_cast<std::chrono::nanoseconds>(timestamp-commandQuery[i].timestamp)).count()>(2.5*1000000000))
            {
                char cmd=commandQuery[i].command;
                std::vector<unsigned char> mess;
                switch(cmd)
                {
                case  ROBOT_VPRED:
                    mess=robot.setTranslationSpeed(commandQuery[i].speed);
                    break;
                case ROBOT_VZAD:
                    mess=robot.setTranslationSpeed(commandQuery[i].speed);
                    break;
                case ROBOT_VLAVO:
                    mess=robot.setRotationSpeed(commandQuery[i].speed);
                    break;
                case ROBOT_VPRAVO:
                    mess=robot.setRotationSpeed(commandQuery[i].speed);
                    break;
                case ROBOT_STOP:
                    mess=robot.setTranslationSpeed(0);
                    break;
                case ROBOT_ARC:
                    mess=robot.setArcSpeed(commandQuery[i].speed,commandQuery[i].radius);
                    break;

                }
                if (sendto(rob_s, (char*)mess.data(), sizeof(char)*mess.size(), 0, (struct sockaddr*) &rob_si_posli, rob_slen) == -1)
                {

                }
                commandQuery.erase(commandQuery.begin()+i);
                i--;

            }
        }
    }
}
*/


void MainWindow::paintThisLidar(LaserMeasurement &laserData)
{
    memcpy( &paintLaserData,&laserData,sizeof(LaserMeasurement));
    updateLaserPicture=1;
    //update();
}

void MainWindow::on_pushButton_8_clicked()//forward
{
    CommandVector help;
    help.command.commandType=1;
    help.command.actualAngle=0;
    help.command.actualDist=0;
    help.command.desiredAngle=0;
    help.command.desiredDist=100;
    struct timespec t;

    // clock_gettime(CLOCK_REALTIME,&t);
    help.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
    AutonomousCommandQuerry.push_back(help);
}

void MainWindow::on_pushButton_10_clicked()//right
{
    CommandVector help;
    help.command.commandType=2;
    help.command.actualAngle=0;
    help.command.actualDist=0;
    help.command.desiredAngle=-20;
    help.command.desiredDist=0;
    struct timespec t;

    // clock_gettime(CLOCK_REALTIME,&t);
    help.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
    AutonomousCommandQuerry.push_back(help);
}

void MainWindow::on_pushButton_11_clicked()//back
{
    CommandVector help;
    help.command.commandType=1;
    help.command.actualAngle=0;
    help.command.actualDist=0;
    help.command.desiredAngle=0;
    help.command.desiredDist=-100;
    struct timespec t;

    //   clock_gettime(CLOCK_REALTIME,&t);
    help.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
    AutonomousCommandQuerry.push_back(help);
}

void MainWindow::on_pushButton_9_clicked()//left
{
    CommandVector help;
    help.command.commandType=2;
    help.command.actualAngle=0;
    help.command.actualDist=0;
    help.command.desiredAngle=20;
    help.command.desiredDist=0;
    struct timespec t;

    //   clock_gettime(CLOCK_REALTIME,&t);
    help.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
    AutonomousCommandQuerry.push_back(help);
}

void MainWindow::on_pushButton_12_clicked()
{

    toleranciaUhla=2;
    naviguj=true;
}

void MainWindow::on_pushButton_13_clicked()
{

}


void MainWindow::skeletonprocess()
{

    std::cout<<"init skeleton"<<std::endl;
#ifdef _WIN32
    WSADATA wsaData = {0};
    int iResult = 0;
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
#else
#endif
    ske_slen = sizeof(ske_si_other);
    if ((ske_s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {

    }

    char ske_broadcastene=1;
#ifdef _WIN32
    DWORD timeout=100;

    std::cout<<setsockopt(ske_s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof timeout)<<std::endl;
    std::cout<<setsockopt(ske_s,SOL_SOCKET,SO_BROADCAST,&ske_broadcastene,sizeof(ske_broadcastene))<<std::endl;
#else
    setsockopt(ske_s,SOL_SOCKET,SO_BROADCAST,&ske_broadcastene,sizeof(ske_broadcastene));
#endif
    // zero out the structure
    memset((char *) &ske_si_me, 0, sizeof(ske_si_me));

    ske_si_me.sin_family = AF_INET;
    ske_si_me.sin_port = htons(23432);
    ske_si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    ske_si_posli.sin_family = AF_INET;
    ske_si_posli.sin_port = htons(23432);
    ske_si_posli.sin_addr.s_addr = inet_addr(ipaddress.data());;//htonl(INADDR_BROADCAST);
    std::cout<<::bind(ske_s , (struct sockaddr*)&ske_si_me, sizeof(ske_si_me) )<<std::endl;;
    char command=0x00;

    skeleton bbbk;
    double measure[225];
    while(stopall==1)
    {

        if ((ske_recv_len = ::recvfrom(ske_s, (char *)&bbbk.joints, sizeof(char)*1800, 0, (struct sockaddr *) &ske_si_other, &ske_slen)) == -1)
        {

        //    std::cout<<"problem s prijatim"<<std::endl;
            continue;
        }


        memcpy(kostricka.joints,bbbk.joints,1800);
     updateSkeletonPicture=1;
     // std::cout<<"doslo "<<ske_recv_len<<std::endl;
     //  continue;
        for(int i=0;i<75;i+=3)
        {
        // std::cout<<klby[i]<<" "<<bbbk.joints[i].x<<" "<<bbbk.joints[i].y<<" "<<bbbk.joints[i].z<<std::endl;
        }
    }
    std::cout<<"koniec thread"<<std::endl;
}

void MainWindow::on_checkBox_2_clicked(bool checked)
{
    showLidar=checked;
}

void MainWindow::on_checkBox_3_clicked(bool checked)
{
    showCamera=checked;
}

void MainWindow::on_checkBox_4_clicked(bool checked)
{
    showSkeleton=checked;
}

void MainWindow::on_checkBox_clicked(bool checked)
{
    applyDelay=checked;
}

void MainWindow::imageViewer()
{
    cv::VideoCapture cap;
    cap.open("http://127.0.0.1:8889/stream.mjpg");
    cv::Mat frameBuf;
    while(1)
    {
        cap >> frameBuf;


        if(frameBuf.rows<=0)
        {
            std::cout<<"nefunguje"<<std::endl;
            continue;
        }

        if(applyDelay==true)
        {
            struct timespec t;
            CameraVector newcommand;
            frameBuf.copyTo(newcommand.data);
            //    clock_gettime(CLOCK_REALTIME,&t);
            newcommand.timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
            auto timestamp=std::chrono::steady_clock::now();//(int64_t)(t.tv_sec) * (int64_t)1000000000 + (int64_t)(t.tv_nsec);
            cameraQuerry.push_back(newcommand);
            for(int i=0;i<cameraQuerry.size();i++)
            {
                if((std::chrono::duration_cast<std::chrono::nanoseconds>(timestamp-cameraQuerry[i].timestamp)).count()>(2.5*1000000000))
                {

                    cameraQuerry[i].data.copyTo(robotPicture);
                    cameraQuerry.erase(cameraQuerry.begin()+i);
                    i--;
                    break;

                }
            }

        }
        else
        {
           frameBuf.copyTo(robotPicture);
        }
        frameBuf.copyTo(AutonomousrobotPicture);
        updateCameraPicture=1;

        update();

       // cv::imshow("client",frameBuf);
        cv::waitKey(1);
        QCoreApplication::processEvents();
    }
}

void MainWindow::on_pushButton_14_clicked()
{
  sendRobotCommand(ROBOT_STOP);
}



